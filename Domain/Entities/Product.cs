﻿namespace Domain.Entities;

public sealed record Product
{
    public required Guid Id { get; init; }
    public required string Name { get; set; }
    public required decimal Price { get; set; }
}