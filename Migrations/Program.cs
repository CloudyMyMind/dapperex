﻿using System.Runtime.ExceptionServices;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using var serviceProvider = CreateServices(new[] 
    { "server=localhost;port=5433;database=DapperEx;username=postgres;password=123456sek;" });
using var scope = serviceProvider.CreateScope();

RunMigrations(scope.ServiceProvider);

static ServiceProvider CreateServices(string[] args)
{
    return new ServiceCollection()
        .AddFluentMigratorCore()
        .ConfigureRunner(runner =>
        {
            runner
                .AddPostgres()
                .WithGlobalConnectionString(GetConnectionString(args))
                .ScanIn(typeof(Program).Assembly)
                .For.Migrations()
                .For.EmbeddedResources();
        })
        .AddLogging(logger => logger.AddFluentMigratorConsole())
        .BuildServiceProvider();
}

static void RunMigrations(IServiceProvider serviceProvider)
{
    MigrateWithRetries(serviceProvider, 5, serviceProvider.GetRequiredService<ILogger<Program>>());
}

static string? GetConnectionString(string[] args)
{
    return args.Length is 1 ? args[0] : Environment.GetEnvironmentVariable("DefaultConnection");
}

static void MigrateWithRetries(
    IServiceProvider serviceProvider,
    int retryCount,
    ILogger logger)
{
    var wait = TimeSpan.FromSeconds(5);
    ExceptionDispatchInfo? edi;
    do
    {
        try
        {
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();
            runner.MigrateUp();
            return;
        }
        catch (Exception exception)
        {
            edi = ExceptionDispatchInfo.Capture(exception);

            logger.LogWarning(
                exception,
                "Error while executing migration. Retrying in {Wait}ms",
                wait.TotalMilliseconds);

            Thread.Sleep(wait);

            wait = wait.Add(TimeSpan.FromSeconds(5));
            retryCount--;
        }
    } while (retryCount > 0);

    edi.Throw();
}

internal partial class Program
{
}
