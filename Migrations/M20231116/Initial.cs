﻿using FluentMigrator;

namespace Migrations.M20231116;

[Migration(20231116_001)]
public sealed class Initial : Migration
{
    public override void Up()
    {
        Create.Table("products")
            .WithColumn("id").AsGuid().PrimaryKey().NotNullable()
            .WithColumn("name").AsString(100).NotNullable().Unique()
            .WithColumn("price").AsDecimal().NotNullable();
    }

    public override void Down()
    {
        Delete.Table("products");
    }
}