﻿using Application.Products.Views;
using MediatR;

namespace Application.Products.Query;

public record GetByIdProductQuery : IRequest<ProductViewModel>
{
    public required Guid Id { get; init; }
}