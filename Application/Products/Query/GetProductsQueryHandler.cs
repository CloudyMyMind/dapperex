﻿using Application.Products.Views;
using Infrastructure.Interfaces;
using MediatR;

namespace Application.Products.Query;

public class GetProductsQueryHandler : IRequestHandler<GetByIdProductQuery, ProductViewModel>
{
    private readonly IUnitOfWork _ofWork;

    public GetProductsQueryHandler(IUnitOfWork ofWork)
    {
        _ofWork = ofWork;
    }

    public async Task<ProductViewModel> Handle(GetByIdProductQuery request, CancellationToken cancellationToken)
    {
        var result = await  _ofWork.ProductRepository.GetByIdAsync(request.Id);
        var newProduct = new ProductViewModel
        {
            Id = result.Id,
            Name = result.Name,
            Price = result.Price
        };
        return newProduct;
    }
}