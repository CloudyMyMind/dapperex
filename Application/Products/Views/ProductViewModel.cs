﻿namespace Application.Products.Views;

public sealed record ProductViewModel
{
    public required Guid Id { get; init; }
    public required string Name { get; init; }
    public required decimal Price { get; init; }
}