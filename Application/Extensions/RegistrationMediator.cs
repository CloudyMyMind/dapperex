﻿using System.Net.Mime;
using Application.Products.Query;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Extensions;

public static class RegistrationMediator
{
    public static void RegistrationMediatorService(this IServiceCollection service)
    {
        service.AddMediatR(typeof(GetByIdProductQuery).Assembly);
    }
}