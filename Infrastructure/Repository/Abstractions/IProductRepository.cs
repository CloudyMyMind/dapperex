﻿using Domain.Entities;
using Infrastructure.Interfaces;

namespace Infrastructure.Repository.Abstractions;

public interface IProductRepository : IGenericRepository<Product>
{
}