﻿using System.Data.Common;
using Dapper;
using Domain.Entities;
using Infrastructure.Repository.Abstractions;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Infrastructure.Repository;

public class ProductRepository : IProductRepository
{
    private readonly IConfiguration _configuration;
    

    public ProductRepository(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public async Task<Product> GetByIdAsync(Guid id)
    {
        try
        {
            var sql = "SELECT * FROM products WHERE Id = @id";
            await using var connect = new NpgsqlConnection(_configuration.GetConnectionString("Default"));
            connect.Open();
            var result = await connect.QueryFirstOrDefaultAsync<Product>(sql, new { Id = id });
            if (result != null) return result;
            throw new Exception("Error");
        }
        catch (Exception e)
        {
            throw new Exception("ex");
        }
    }

    public Task<Product> GetAllAsync()
    {
        throw new NotImplementedException();
    }

    public Task<Product> AddAsync(Product entity)
    {
        throw new NotImplementedException();
    }

    public Task<Product> UpdateAsync(Product entity)
    {
        throw new NotImplementedException();
    }

    public Task<bool> DeleteAsync(Guid entity)
    {
        throw new NotImplementedException();
    }

    public Task<bool> AnyAsync(Guid entity)
    {
        throw new NotImplementedException();
    }
}