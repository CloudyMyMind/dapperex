﻿using Infrastructure.Interfaces;
using Infrastructure.Repository.Abstractions;

namespace Infrastructure.Repository;

public class UnitOfWork : IUnitOfWork
{
  
    public UnitOfWork(IProductRepository productRepository)
    {
        ProductRepository = productRepository;
    }
    public IProductRepository ProductRepository { get; }
}