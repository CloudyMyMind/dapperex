﻿using Infrastructure.Repository.Abstractions;

namespace Infrastructure.Interfaces;

public interface IUnitOfWork
{
    public IProductRepository ProductRepository { get; }
}