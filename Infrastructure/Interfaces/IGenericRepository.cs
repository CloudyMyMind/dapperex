﻿namespace Infrastructure.Interfaces;

public interface IGenericRepository<T> where T : class
{
    Task<T> GetByIdAsync(Guid id);
    Task<T> GetAllAsync();
    Task<T> AddAsync(T entity);
    Task<T> UpdateAsync(T entity);
    Task<bool> DeleteAsync(Guid entity);
    Task<bool> AnyAsync(Guid entity);
    // void GetAll();
    // void Add(T entity);
    // void Update(T entity);
    // void Delete(T entity);
    // void Any(T entity);
}