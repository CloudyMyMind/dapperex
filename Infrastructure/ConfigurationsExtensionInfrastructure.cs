﻿using Infrastructure.Interfaces;
using Infrastructure.Repository;
using Infrastructure.Repository.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure;

public static class ConfigurationsExtensionInfrastructure
{

    public static void RegistrationRepository(this IServiceCollection service)
    {
        service.AddScoped<IProductRepository, ProductRepository>();
        service.AddTransient<IUnitOfWork, UnitOfWork>();
    }
}