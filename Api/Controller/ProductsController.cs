﻿using Application.Products.Query;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controller;

[ApiController]
[Route("api/[controller]")]
public class ProductsController : ControllerBase
{
    private readonly IMediator _mediator;

    public ProductsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<IActionResult> GetById(Guid id)
    {
        var result = await _mediator.Send(new GetByIdProductQuery{ Id = id});
        return Ok(result);
    }
}